import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VueGoogleHeatmap from "vue-google-heatmap";

Vue.use(VueGoogleHeatmap, {
  apiKey: "AIzaSyCZ3g_OmYl9EvAi1a3CB2k6Z48mrJHSYSs"
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
